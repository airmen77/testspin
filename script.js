var slotWidth = 100;
var sprite = new Image();
var c = document.getElementById("Slots");
var ctx = c.getContext("2d");
sprite.src = "css_sprites.png";

const item = {
    one: {
        x: 2662,
        y: 10,
        w: 378,
        h: 475
    },
    two: {
        x: 1589,
        y: 1510,
        w: 470,
        h: 437
    },
    tree: {
        x: 10,
        y: 2115,
        w: 449,
        h: 448
    },
    four: {
        x: 1127,
        y: 1510,
        w: 449,
        h: 448
    },
    five: {
        x: 642,
        y: 1510,
        w: 442,
        h: 473
    },
    six: {
        x: 2662,
        y: 505,
        w: 346,
        h: 379
    },
    seven: {
        x: 2662,
        y: 904,
        w: 346,
        h: 379
    },
    eight: {
        x: 2662,
        y: 1270,
        w: 342,
        h: 346
    },
    nine: {
        x: 2662,
        y: 1676,
        w: 246,
        h: 348
    },
    ten: {
        x: 2079,
        y: 1510,
        w: 246,
        h: 348
    }
};
const param = [
    [item.nine,
        item.tree,
        item.one,
        item.two,
        item.one,
        item.five,
        item.four,
        item.one,
        item.two,
        item.four,
        item.one,
        item.tree,
        item.one,
        item.six,
        item.tree,
        item.one
    ],
    [item.nine,
        item.one,
        item.tree,
        item.one,
        item.two,
        item.four,
        item.one,
        item.five,
        item.tree,
        item.one,
        item.six,
        item.four,
        item.one,
        item.two,
        item.five,
        item.one
    ],
    [item.nine,
        item.tree,
        item.one,
        item.two,
        item.one,
        item.five,
        item.four,
        item.one,
        item.two,
        item.four,
        item.one,
        item.tree,
        item.one,
        item.six,
        item.tree,
        item.one
    ],
    [item.nine,
        item.one,
        item.two,
        item.four,
        item.one,
        item.tree,
        item.one,
        item.five,
        item.one,
        item.six,
        item.two,
        item.one,
        item.four,
        item.five,
        item.one,
        item.tree
    ],
    [item.nine,
        item.tree,
        item.one,
        item.two,
        item.one,
        item.five,
        item.four,
        item.one,
        item.two,
        item.four,
        item.one,
        item.tree,
        item.one,
        item.six,
        item.tree,
        item.one
    ]
];

var exten = param[0].length * slotWidth;

var slots = [new Slot(50, 10), new Slot(50, 10), new Slot(50, 10), new Slot(50, 10), new Slot(50, 10)];

c.width = slots.length * (slotWidth + 20);

function Slot(max, step) {
    this.speed = 0; //скорость слота
    this.step = step; //коеф. увеличения скорости
    this.si = null; //индификатор setInterval объекта
    this.pos = 100;
    this.started = false;
    this.maxSpeed = max; //максимальная скорость слота
}

function spin(i) {
    for (var u = 0; u < param[i].length; u++) {
        if (slots[i].pos > exten) {
            slots[i].pos = 0;
        }
        ctx.drawImage(sprite, param[i][u].x, param[i][u].y, param[i][u].w, param[i][u].h, 120 * i + 10, 100 * (u) + slots[i].pos, 100, 100);
        ctx.drawImage(sprite, param[i][u].x, param[i][u].y, param[i][u].w, param[i][u].h, 120 * i + 10, 100 * (u) - (exten - slots[i].pos), 100, 100)
    }
}

function updateGreed() {
    ctx.clearRect(0, 0, c.width, c.height);
    for (var i = 0; i < slots.length; i++) {
        ctx.strokeStyle = "#000";
        ctx.strokeRect(120 * i + 10, 0, 100, 300);
        spin(i);
    }
    ctx.clearRect(0, 0, c.width, 0);
    ctx.clearRect(0, c.height, c.width, 10);
}

function next() {
    slots[0].next = ()=>{setTimeout(()=>{slots[1].stop();},400)};
    slots[1].next = ()=>{setTimeout(()=>{slots[2].stop();},350)};
    slots[2].next = ()=>{setTimeout(()=>{slots[3].stop();},200)};
    slots[3].next = ()=>{setTimeout(()=>{slots[4].stop();},100)};
}

function getRInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

Slot.prototype.start = function(pos) {
    var _this = this;

    _this.finalPos = pos;


    if (!_this.started) {
        _this.started = true;

        _this.si = window.setInterval(function() {
            if (_this.speed < _this.maxSpeed) {
                _this.speed += _this.step;
            }
            _this.pos += _this.speed;
        }, 30);
    }
};

Slot.prototype.pause = function(pos) {
    var _this = this;

    _this.finalPos = pos;

    clearInterval(_this.si);
    _this.speed = 0;
    _this.started = false;
};


Slot.prototype.stop = function() {
    var _this = this,
        rand = (Math.floor(Math.random() * 5) + 4) * 100;
    limit = 10;
    clearInterval(_this.si);
    _this.si = window.setInterval(function() {
        if (_this.speed > limit) {
            if (_this.pos >= _this.finalPos - rand && _this.finalPos - (rand > 0)) {
                _this.speed -= _this.step
            } else if (_this.pos >= exten + (_this.finalPos - rand) && _this.finalPos - rand < 0) {
                _this.speed -= _this.step
            }
        } else {
            if (_this.pos == _this.finalPos) {
                clearInterval(_this.si);
                _this.started = false;
                if (_this.hasOwnProperty('next')){_this.next();}
                _this.pos -= 10;
            }
        }

        _this.pos += _this.speed;

    }, 30);

};


for (var i = 0; i < slots.length; i++) {
    ctx.strokeStyle = "#000";
    ctx.strokeRect((slotWidth + 20) * i + 10, 10, 100, 300);
    ctx.save();
}

for (var i = 0; i < slots.length; i++) {
    slots[i].start(getRInt(0, 15) * 100);
}

setInterval(function() {
    updateGreed();
}, 30);


document.getElementById("freeze").addEventListener('click',function() {
    for (var i = 0; i < slots.length; i++) {
        slots[i].pause(getRInt(0, 15) * 100);
    }

});

document.getElementById("spin").addEventListener('click',function() {
    for (var i = 0; i < slots.length; i++) {
        slots[i].start(getRInt(0, 15) * 100);
    }
    next();

    setTimeout(()=>{slots[0].stop();},550);

});